import 'dart:ui';

import 'package:bonch_personal_area/static/export.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class CabinetController {
  InAppWebViewController _controller;

  CabinetController(this._controller);

  Future goTimetable() async {
    return _controller.evaluateJavascript(source: """
      document.querySelector("a[title='Расписание']").click()
    """);
  }

  Future goMessages() async {
    return _controller.evaluateJavascript(source: """
      document.querySelector("a[title='Сообщения']").click()
    """);
  }

  Future goFiles() async {
    return _controller.evaluateJavascript(source: """
      document.querySelector("a[title='ФАЙЛЫ ГРУППЫ']").click()
    """);
  }

  Future login(String login, String password) async {
    print("Login to cabinet with $login");
    return _controller.evaluateJavascript(source: """
      document.querySelector("#users").value = "$login"
      document.querySelector("#parole").value = "$password"
      putLoginTo(this)
    """);
  }

  Future logoutConfirm() async {
    return _controller.evaluateJavascript(source: """
      putLoginNull(this)
    """);
  }

  Future goToBonch() async {
    if (!(await _controller.getUrl()).startsWith(BonchLinks.origin))
      _controller.loadUrl(url: BonchLinks.cabinetUrl);
  }

  Future setupPage({
    List<String> excludeToggleDrawerMenus,
    Size pageSize,
  }) async {
    excludeToggleDrawerMenus = excludeToggleDrawerMenus ?? [];
    final excludeString = excludeToggleDrawerMenus.map((e) => '"$e"').join(",");

    await _controller.injectCSSCode(source: """
      #header, #logotip, #exit { display: none; }
      #rightpanel, #leftpanel { top: 0; }
    """);

    if (pageSize != null) {
      await _controller.injectCSSCode(source: """
        #rightpanel {
          height: ${pageSize.height}px !important;
          width: ${pageSize.width}px !important;
        }
        #leftpanel, .left_bar {
          background-color: white !important;
          height: ${pageSize.height}px !important;
          width: ${pageSize.width * 0.75}px !important;
        }
        .l_menu_a, .title_item font, .title_item a:visited {
          color: #212121 !important;
        }
        .title_item:not(.collapsed), .title_item:hover {
          background-color: #ffcc80 !important;
        }
        .collapse.show, .collapsing {
          background-color: #f5f5f5 !important;
        }
        .active_menu {
          background-color: #ffe0b2 !important;
        }
      """);
    }

    return _controller.evaluateJavascript(source: """
      let drawer = document.querySelector("#mob_menu_lnk")
      document.querySelectorAll("a.l_menu_a").forEach((e) => {
        if (![$excludeString].includes(e.getAttribute("title"))) {
          e.addEventListener('click', () => { drawer.click() })
        } else {
          e.style.display = "none"
        }
      })
    """);
  }

  Future toggleDrawer() async {
    return _controller.evaluateJavascript(source: """
      document.querySelector("#mob_menu_lnk").click()
    """);
  }
}