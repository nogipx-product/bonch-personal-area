class BonchLinks {

  BonchLinks._();

  static const host = "lk.sut.ru";
  static const origin = "https://$host";
  static const logoUrl = "https://upload.wikimedia.org/wikipedia/commons/8/81/%D0%9B%D0%BE%D0%B3%D0%BE_%D1%86%D0%B2.png";
  static const bgUrl = "https://lk.sut.ru/cabinet/ini/general/0/cabinet/img/bg_n.jpg";

  static const cabinetUrl = "https://lk.sut.ru/cabinet";
  static const cabinetUrlUnauthorized = "https://lk.sut.ru/?login=no";
  static const cabinetLoginUrl = "$cabinetUrl/lib/autentificationok.php?";

  static const fileFormats = ["pdf", "docx", "doc", "ppt", "pptx"];
}