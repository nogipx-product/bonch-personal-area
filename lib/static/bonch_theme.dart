import 'package:flutter/material.dart';
import 'package:flutter_google_ui/flutter_google_ui.dart';

class BonchTheme {

  BonchTheme._();

  static final spbgutColor = Color(0xffef6c37);
  static final spbgutOtherColor = Color(0xffef7437);

  static final theme = ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.orange,
    highlightColor: Colors.black.selected,
    focusColor: Colors.black.focus,
    hoverColor: Colors.black.hover,
    splashColor: Colors.black.pressed,
    primaryColor: Colors.orange,
    buttonTheme: ButtonThemeData(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      // focusColor: Colors.orange.focus,
      // highlightColor: Colors.orange.selected,
      // hoverColor: Colors.orange.hover,
      // splashColor: Colors.orange.pressed
    ),
    inputDecorationTheme: InputDecorationTheme(
      fillColor: Colors.orange,
      focusColor: Colors.orange.focus,
      hoverColor: Colors.orange.hover,
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(width: 1, color: Colors.orange)
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(width: .5, color: Colors.black45)
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(width: .5, color: Colors.black45)
      ),
      // labelStyle: Theme.of(context).textTheme.caption.copyWith(
      //   fontWeight: FontWeight.bold
      // ),
    )
  );

}