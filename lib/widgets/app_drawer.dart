import 'package:bonch_personal_area/controller/export.dart';
import 'package:flutter/material.dart';

class BonchDrawer extends StatelessWidget {
  final CabinetController _cabinetProvider;

  const BonchDrawer({Key key, 
    @required CabinetController cabinetProvider
  }): _cabinetProvider = cabinetProvider,
      super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: _cabinetProvider == null
        ? Center(child: CircularProgressIndicator())
        : ListView(
            shrinkWrap: true,
            children: [
              DrawerHeader(child: null),
              ListTile(
                title: Text("Расписание"),
                onTap: () {
                  Navigator.of(context).pop();
                  _cabinetProvider.goTimetable();
                } 
              )
            ],
          ),
    );
  }
}