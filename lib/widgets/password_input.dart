import 'package:flutter/material.dart';

class PasswordInputField extends StatefulWidget {
  final String labelText;
  final bool forceObscure;
  final TextEditingController controller;
  final String errorText;
  final InputDecoration decoration;

  const PasswordInputField({Key key, 
    this.decoration,
    this.labelText, this.forceObscure, this.controller, this.errorText
  }) : super(key: key);

  @override
  _PasswordInputFieldState createState() => _PasswordInputFieldState();
}

class _PasswordInputFieldState extends State<PasswordInputField> {

  bool _isObscured = true;

  @override
  Widget build(BuildContext context) {
    final _obscureText = widget.forceObscure ?? _isObscured;

    return TextFormField(
      obscureText: _obscureText,
      obscuringCharacter: "*",
      controller: widget.controller,
      decoration: InputDecoration(
        labelText: widget.labelText,
        suffixIcon: Visibility(
          visible: widget.forceObscure == null,
          child: IconButton(
            icon: _obscureText 
              ? Icon(Icons.visibility_off, color: Colors.black45)
              : Icon(Icons.visibility, color: Colors.black87),
            onPressed: () {
              setState(() {
                _isObscured = !_isObscured;
              });
            },
          ),
        )
      )
    );
  }
}