import 'package:flutter/material.dart';

extension LogoutDialog on BuildContext {
  Future<bool> showLogoutDialog({
    String title = "Выйти?"
  }) => showDialog<bool>(
    context: this,
    useRootNavigator: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        actions: [
          FlatButton(
            onPressed: () =>
              Navigator.of(context).pop(false),
            child: Text("Остаться".toUpperCase())
          ),
          FlatButton(
            onPressed: () =>
              Navigator.of(context).pop(true), 
            child: Text("Выйти".toUpperCase())
          )
        ],
      );
    }
  );
}