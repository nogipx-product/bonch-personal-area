import 'package:bonch_personal_area/static/icons/bonch_icons.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

extension AuthorDialog on BuildContext {
  

  Future<bool> showAboutDialog() => showDialog<bool>(
    context: this,
    useRootNavigator: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("Bonch.Personal"),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppInfo(),
            Divider(height: 24),
            NogipxInfo()
          ],
        ),
        actions: [
          FlatButton(
            onPressed: () =>
              Navigator.of(context).pop(true), 
            child: Text("OK".toUpperCase())
          )
        ],
      );
    }
  );
}

class AppInfo extends StatelessWidget {
  static const repository = "https://gitlab.com/nogipx-product/bonch-personal-area";

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Align(
          alignment: Alignment.topLeft,
          child: Text("This is open source software. Feel free to contribute 🧡")
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton.icon(
            icon: Icon(BonchIcon.gitlab, color: Colors.white),
            label: Text("Gitlab"),
            onPressed: () {
              launch(repository);
            }
          ),
        )
      ],
    );
  }
}

class NogipxInfo extends StatelessWidget {
  static const emailHash = "62689a8effa882e490a7779a0721b346";
  static const email = "nogipx@gmail.com";
  static const vk = "https://vk.com/nogipx";
  static const gitlab = "https://gitlab.com/nogipx";
  static const telegram = "https://t.me/nogipx";
  static const instagram = "https://instagram.com/nogipx";
  static const osi = "https://gitlab.com/nogipx-product";
  static const steam = "https://steamcommunity.com/id/nogipx";
  static const spotify = 
    "https://open.spotify.com/user/13mq7qrawquzl6ierb33f359h?si=aMWCNfruTGKFJJwz8HO9-A";
  
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: CachedNetworkImage(
                placeholder: (_, __) => CircularProgressIndicator(),
                imageUrl: "https://www.gravatar.com/avatar/${NogipxInfo.emailHash}"
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Разработчик",
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        "Mamatkazin Karim (@nogipx)",
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                          fontStyle: FontStyle.italic
                        )
                      ),
                    ),
                    Divider(),
                  ]
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 12),
        ExpansionTile(
          leading: Icon(Icons.link_rounded),
          title: Align(
            alignment: Alignment.centerLeft,
            child: Text("Сссылки на профили"),
          ),
          children: [
            Wrap(
              spacing: 12,
              children: [
                [gitlab, BonchIcon.gitlab, Colors.deepOrange],
                [vk, BonchIcon.vk, Colors.blue],
                [telegram, BonchIcon.telegram_plane, Colors.lightBlue],
                [instagram, BonchIcon.instagram, Colors.pink],
                [osi, BonchIcon.osi, Colors.deepOrange],
                [spotify, BonchIcon.spotify, Colors.green],
                [steam, BonchIcon.steam, Colors.blueGrey],
                ["mailto: $email", Icons.email_rounded, Colors.grey],
              ].map((data) => IconButton(
                icon: Icon(data[1], color: data[2]),
                onPressed: () => launch(data[0]),
              )).toList()
            ),
          ]
        )
      ],
    );
  }
}