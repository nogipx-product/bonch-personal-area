import 'dart:convert';

import 'package:bonch_personal_area/provider/export.dart';
import 'package:bonch_personal_area/static/bonch_links.dart';
import 'package:http/http.dart' as http;

class CabinetProvider {

  Future<bool> checkCredentials(Credentials credentials) async {
    return http.post(
      BonchLinks.cabinetLoginUrl, 
      body: jsonEncode(credentials.toJson())
    ).then((response) {
      print(response.body);
      if (response.statusCode == 200) {
        
      }
    });
  }

}