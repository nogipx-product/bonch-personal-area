import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class CredentialsProvider {

  FlutterSecureStorage _storage;
  static const _key = "credentials";

  CredentialsProvider(): _storage = FlutterSecureStorage();

  saveCredentials(Credentials credentials) async {
    await _storage.write(
      key: _key, 
      value: jsonEncode(credentials.toJson())
    );
  }

  Future<Credentials> getCredentials() async {
    final credentialsString = await _storage.read(key: _key);

    return credentialsString != null 
      ? Credentials.fromJson(jsonDecode(credentialsString))
      : null;
  }

  clearCredentials() async {
    await _storage.delete(key: _key);
  }

}

class Credentials {
  final String login;
  final String password;

  Credentials({
    @required this.login, 
    @required this.password
  });

  factory Credentials.fromJson(Map<String, dynamic> json) =>
    Credentials(
      login: json["login"],
      password: json["password"]
    );

  Map<String, dynamic> toJson() => {
    "login": login,
    "password": password
  };
}