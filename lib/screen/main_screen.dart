import 'dart:async';

import 'package:bonch_personal_area/screen/cabinet_web_screen.dart';
import 'package:bonch_personal_area/screen/export.dart';
import 'package:bonch_personal_area/static/export.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  StreamController<bool> _cabinetStatus;

  @override
  void initState() {
    _cabinetStatus = StreamController.broadcast();
    super.initState();
  }

  @override
  void dispose() {
    _cabinetStatus.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      stream: _cabinetStatus.stream,
      initialData: false,
      builder: (context, snapshot) {
        return Stack(
          clipBehavior: Clip.none,
          children: [
            BonchCabinetWeb(
              onInitComplete: (controller) async {
                final url = await controller.getUrl();
                if (url.startsWith("${BonchLinks.cabinetUrl}/?login=yes"))
                  _cabinetStatus.add(true);
              }
            ),
            Visibility(
              visible: !snapshot.data,
              child: LoadingScreen()
            ),
          ],
        );
      }
    );
  }
}