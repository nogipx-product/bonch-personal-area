import 'package:bonch_personal_area/provider/export.dart';
import 'package:bonch_personal_area/static/export.dart';
import 'package:bonch_personal_area/widgets/password_input.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {

  final Function(Credentials) onLogin;

  const LoginScreen({Key key, 
    this.onLogin
  }) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  TextEditingController _login;
  TextEditingController _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24),
        child: FutureBuilder<Credentials>(
          future: context.repository<CredentialsProvider>().getCredentials(),
          builder: (context, snapshot) {
            final credentials = snapshot.data;
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CachedNetworkImage(
                          placeholder: (_, __) => CircularProgressIndicator(),
                          imageUrl: BonchLinks.logoUrl,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "Личный кабинет и СДО",
                            style: Theme.of(context).textTheme.subtitle1.copyWith(
                              fontStyle: FontStyle.italic
                            )
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Divider(),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: TextFormField(
                    controller: _login..text = credentials?.login,
                    decoration: InputDecoration(
                      labelText: "Логин"
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: PasswordInputField(
                    labelText: "Пароль",
                    controller: _password..text = credentials?.password,
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: FlatButton.icon(
                        onPressed: () => _loginAction(context,
                          _login.text, _password.text
                        ), 
                        icon: Icon(BonchIcon.login), 
                        label: Text("Войти")
                      ),
                    ),
                  ],
                )
              ],
            );
          }
        ),
      ),
    );
  }

  _loginAction(BuildContext context, String login, String password) {
    if (login != null && login.isNotEmpty &&
        password != null && password.isNotEmpty
    ) {
      final credentials = Credentials(
        login: _login.text,
        password: _password.text
      );
      context.repository<CredentialsProvider>()
        .saveCredentials(credentials);

      Navigator.pushNamed(context, "/cabinet");
    }
  }

    @override
  void initState() {
    _login = TextEditingController();
    _password =  TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _login.dispose();
    _password.dispose();
    super.dispose();
  }

}