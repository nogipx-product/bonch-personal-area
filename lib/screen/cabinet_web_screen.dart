import 'package:bonch_personal_area/controller/export.dart';
import 'package:bonch_personal_area/provider/export.dart';
import 'package:bonch_personal_area/static/export.dart';
import 'package:bonch_personal_area/widgets/export.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';

class BonchCabinetWeb extends StatefulWidget {
  final Widget child;
  final Function(InAppWebViewController) onInitComplete;

  const BonchCabinetWeb({Key key, 
    this.child, 
    this.onInitComplete
  }) : super(key: key);

  @override
  _BonchCabinetWebState createState() => _BonchCabinetWebState();
}

class _BonchCabinetWebState extends State<BonchCabinetWeb> {
  CabinetController _cabinet;

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu_rounded),
          onPressed: () async => _cabinet.toggleDrawer(),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.folder_open_rounded), 
            onPressed: () async {
              _cabinet.goToBonch();
              _cabinet.goFiles();
            }
          ),
          IconButton(
            icon: Icon(Icons.mail_outline_rounded), 
            onPressed: () async {
              _cabinet.goToBonch();
              _cabinet.goMessages();
            }
          ),
          IconButton(
            icon: Icon(Icons.event_rounded), 
            onPressed: () async {
              _cabinet.goToBonch();
              _cabinet.goTimetable();
            }
          ),
          PopupMenuButton(
            icon: Icon(Icons.settings_rounded),
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: FlatButton.icon(
                    minWidth: 90,
                    shape: RoundedRectangleBorder(),
                    label: Text("О приложении"), 
                    icon: Icon(Icons.info_outline_rounded),
                    onPressed: () async {
                      Navigator.of(context).pop();
                      context.showAboutDialog();
                    }
                  )
                ),
                PopupMenuItem(
                  child: FlatButton.icon(
                    minWidth: 90,
                    shape: RoundedRectangleBorder(),
                    label: Text("Выйти"), 
                    icon: Icon(BonchIcon.logout),
                    onPressed: () async {
                      Navigator.of(context).pop();
                      _exit(context);
                    }
                  )
                ),
              ];
            }
          )
        ],
      ),
      body: InAppWebView(
        initialOptions: InAppWebViewGroupOptions(
          crossPlatform: InAppWebViewOptions(
            clearCache: true,
            useShouldOverrideUrlLoading: true,
            verticalScrollBarEnabled: true,
            horizontalScrollBarEnabled: false
          )
        ),
        initialUrl: BonchLinks.cabinetUrlUnauthorized,
        onConsoleMessage: (controller, m) => 
          print("[CABINET] ${m.messageLevel}: ${m.message}"),
        shouldOverrideUrlLoading: (controller, request) =>
          _overrideUrlLoading(controller, request),
        onWebViewCreated: (controller) =>
          _onWebViewCreated(context, controller),
        onLoadStop: (controller, _) {
          _onLoadEnd(context, controller);
          widget.onInitComplete?.call(controller);
        },
        onJsConfirm: (controller, request) =>
          _onJsConfirm(controller, request),
      ),
    );
  }

  Future _exit(
    BuildContext context,
  ) async {
    return context.showLogoutDialog(
      title: "Выйти из личного кабинета?"
    ).then((confirmLogout) {
      if (confirmLogout) {
        context.repository<CredentialsProvider>().clearCredentials();
        Navigator.of(context).pushNamedAndRemoveUntil(
          "/login", (route) => false);
      }
    });
  }

  _onLoadEnd(BuildContext context, InAppWebViewController controller) async {
    final mediaQuery = MediaQuery.of(context);
    _cabinet.setupPage(
      excludeToggleDrawerMenus: ["Расписание", "Сообщения", "ФАЙЛЫ ГРУППЫ"],
      pageSize: Size(
        mediaQuery.size.width,
        mediaQuery.size.height - kToolbarHeight - 20
      ) * 0.999
    );
    _cabinet.goTimetable();
    final url = Uri.tryParse(await controller.getUrl());

    // Needed workaround with additonal slash
    if (url != null && url.toString() == "${BonchLinks.cabinetUrl}/") {
      _onWebViewCreated(context, controller);
    }
  }

  _onWebViewCreated(BuildContext context, InAppWebViewController controller) async {
    final credentials = await context
      .repository<CredentialsProvider>()
      .getCredentials();

    _cabinet = CabinetController(controller);

    if (credentials != null) {
      _cabinet.login(credentials.login, credentials.password);
    }
  }

  Future<ShouldOverrideUrlLoadingAction> _overrideUrlLoading(
    InAppWebViewController controller, 
    ShouldOverrideUrlLoadingRequest request
  ) async {
    if (
      !request.url.startsWith(BonchLinks.origin) 
      && await canLaunch(request.url)
      || BonchLinks.fileFormats.any((f) => request.url.endsWith(f))
    ) {
      launch(request.url);
      return ShouldOverrideUrlLoadingAction.CANCEL;
    } else {
      print("[CABINET] LOAD: ${request.url}");
    }

    return ShouldOverrideUrlLoadingAction.ALLOW;
  }

  _onJsConfirm(InAppWebViewController controller, JsConfirmRequest request) {}
}