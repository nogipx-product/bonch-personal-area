import 'package:bonch_personal_area/static/export.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Positioned.fill(
              child: CachedNetworkImage(
                fit: BoxFit.cover,
                imageUrl: BonchLinks.bgUrl
              ),
            ),
            Center(
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 32, horizontal: 24 
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: Text(
                          "Делаю личный кабинет удобнее...",
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.headline6.copyWith(
                          ),
                        )
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 32.0),
                        child: CircularProgressIndicator(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: FutureBuilder<bool>(
                          future: Future.delayed(Duration(seconds: 15)).then((_) => true),
                          builder: (context, snapshot) {
                            return Visibility(
                              visible: snapshot.data ?? false,
                              child: Column(
                                children: [
                                  Text(
                                    "Загружается дольше обычного. Проверь, пожалуйста, соединение. 😊",
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(height: 12),
                                  OutlinedButton.icon(
                                    onPressed: () => Navigator.of(context)
                                      .pushNamedAndRemoveUntil("/login", (route) => false),
                                    icon: Icon(Icons.logout),
                                    label: Text("Или вернись и проверь пароль 🤔"),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}