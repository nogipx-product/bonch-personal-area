import 'package:bonch_personal_area/provider/cabinet_provider.dart';
import 'package:bonch_personal_area/provider/credentials_provider.dart';
import 'package:bonch_personal_area/screen/export.dart';
import 'package:bonch_personal_area/static/export.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    final credentialProvider = CredentialsProvider();

    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => credentialProvider
        ),
        RepositoryProvider(
          create: (context) => CabinetProvider()
        )
      ], 
      child: MaterialApp(
        title: 'Bonch.Personal',
        theme: BonchTheme.theme,
        debugShowCheckedModeBanner: false,
        routes: {
          "/login": (_) => LoginScreen(),
          "/cabinet": (_) => MainScreen()
        },
        initialRoute: credentialProvider.getCredentials() != null
          ? "/cabinet"
          : "/login",
      ),
    );
  }
}


